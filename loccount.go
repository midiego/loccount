// SPDX-License-Identifier: BSD-2-Clause
package main

//go:generate ./tablegen.py -g traits.json traits.go

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io"
	"io/ioutil"
	"log"
	"math"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"runtime/pprof"
	"sort"
	"strings"
	"sync"
)

var version string

// Following code swiped from Michael T. Jones's "walk" package.
// It's a parallelized implementation of tree-walking that's
// faster than the version in the system filepath library.
// Note, however, it seems to have a limitation - does not like paths
// containing "..".

type visitData struct {
	path string
	info os.FileInfo
}

// WalkFunc is the type of the function called for each file or directory
// visited by Walk. The path argument contains the argument to Walk as a
// prefix; that is, if Walk is called with "dir", which is a directory
// containing the file "a", the walk function will be called with argument
// "dir/a". The info argument is the os.FileInfo for the named path.
//
// If there was a problem walking to the file or directory named by path, the
// incoming error will describe the problem and the function can decide how
// to handle that error (and Walk will not descend into that directory). If
// an error is returned, processing stops. The sole exception is that if path
// is a directory and the function returns the special value SkipDir, the
// contents of the directory are skipped and processing continues as usual on
// the next file.
type WalkFunc func(path string, info os.FileInfo, err error) error

type walkState struct {
	walkFn     WalkFunc
	v          chan visitData // files to be processed
	active     sync.WaitGroup // number of files to process
	lock       sync.RWMutex
	firstError error // accessed using lock
}

func (ws *walkState) terminated() bool {
	ws.lock.RLock()
	done := ws.firstError != nil
	ws.lock.RUnlock()
	return done
}

func (ws *walkState) setTerminated(err error) {
	ws.lock.Lock()
	if ws.firstError == nil {
		ws.firstError = err
	}
	ws.lock.Unlock()
	return
}

func (ws *walkState) visitChannel() {
	for file := range ws.v {
		ws.visitFile(file)
		ws.active.Add(-1)
	}
}

// readDirNames reads the directory named by dirname and returns
// a sorted list of directory entries.
func readDirNames(dirname string) ([]string, error) {
	f, err := os.Open(dirname)
	if err != nil {
		return nil, err
	}
	names, err := f.Readdirnames(-1)
	f.Close()
	if err != nil {
		return nil, err
	}
	sort.Strings(names) // omit sort to save 1-2%
	return names, nil
}

func (ws *walkState) visitFile(file visitData) {
	if ws.terminated() {
		return
	}

	err := ws.walkFn(file.path, file.info, nil)
	if err != nil {
		if !(file.info.IsDir() && err == filepath.SkipDir) {
			ws.setTerminated(err)
		}
		return
	}

	if !file.info.IsDir() {
		return
	}

	names, err := readDirNames(file.path)
	if err != nil {
		err = ws.walkFn(file.path, file.info, err)
		if err != nil {
			ws.setTerminated(err)
		}
		return
	}

	here := file.path
	for _, name := range names {
		file.path = filepath.Join(here, name)
		file.info, err = os.Lstat(file.path)
		if err != nil {
			err = ws.walkFn(file.path, file.info, err)
			if err != nil && (!file.info.IsDir() || err != filepath.SkipDir) {
				ws.setTerminated(err)
				return
			}
		} else {
			switch file.info.IsDir() {
			case true:
				ws.active.Add(1) // presume channel send will succeed
				select {
				case ws.v <- file:
					// push directory info to queue for concurrent traversal
				default:
					// undo increment when send fails and handle now
					ws.active.Add(-1)
					ws.visitFile(file)
				}
			case false:
				err = ws.walkFn(file.path, file.info, nil)
				if err != nil {
					ws.setTerminated(err)
					return
				}
			}
		}
	}
}

// Walk walks the file tree rooted at root, calling walkFn for each file or
// directory in the tree, including root. All errors that arise visiting files
// and directories are filtered by walkFn. The files are walked in a random
// order. walk does not follow symbolic links.

func walk(root string, walkFn WalkFunc) error {
	info, err := os.Lstat(root)
	if err != nil {
		return walkFn(root, nil, err)
	}

	ws := &walkState{
		walkFn: walkFn,
		v:      make(chan visitData, 1024),
	}
	defer close(ws.v)

	ws.active.Add(1)
	ws.v <- visitData{root, info}

	walkers := 16
	for i := 0; i < walkers; i++ {
		go ws.visitChannel()
	}
	ws.active.Wait()

	return ws.firstError
}

// Swiped code ends here

// SourceStat - line count record for a specified path
type SourceStat struct {
	Path     string
	Language string
	SLOC     uint
	LLOC     uint
	markup   bool
}

func (s SourceStat) nonEmpty() bool {
	return s.SLOC > 0
}

var debug int
var transdump bool
var listignores bool
var exclusions *regexp.Regexp
var pipeline chan SourceStat

// Data table driving the recognition and counting of languages.
// Initialized frm traits.json at build time; see the comments there.

type genericLanguage struct {
	Name     string      // Language name
	Ext      []string    // Extensions, with leading dot
	Bcl      string      // Leader string for block comments
	Bct      string      // Trailer string for block comments
	Wcl      string      // Leader string for winged comments
	Ml       [][2]string // Delimiter pairs for multiline literals
	quirks   uint        // Quirk flags
	St       string      // Statement-terminating punctuation
	verifier func(*countContext, string, string) bool
}

func (g genericLanguage) property(v uint) bool {
	return (v & g.quirks) != 0
}

func (g genericLanguage) suffixMatches(path string) string {
	for _, s := range g.Ext {
		if strings.HasSuffix(path, s) {
			return s
		}
	}
	return ""
}

func (g genericLanguage) matchables() []string {
	m := []string{strings.ToLower(g.Name)}
	for _, s := range g.Ext {
		m = append(m, s[1:])
	}
	return m
}

type literalBracket struct {
	quirk uint
	start string
	end   string
}

var literalBrackets []literalBracket

const dt = `"""`
const st = `'''`

var dtriple, striple, dtrailer, strailer, dlonely, slonely *regexp.Regexp

var podheader *regexp.Regexp

var neverInterestingByPrefix []string
var actuallyInterestingByPrefix []string
var neverInterestingByInfix []string
var neverInterestingBySuffix map[string]bool
var neverInterestingByBasename map[string]bool

var cHeaderPriority []string
var generated string

// Syntax quirk flags.  Those with an x prefix don't currently affect parsing,
// they're declared to support things we might want to do in the future.
//
// Note that by default both ' and " are treated as quotes when enclosing
// comment starts or ends; you have to give dst/sst/nostring to change this.
const (
	nf     = 0         // No flags
	asm    = 1 << iota // Assembler syntax: handle multiple winged-comment types
	bexp               // Language has bare regexp literals
	cbs                // C-style backslash escapes
	cnest              // Comments nest
	crl                // Has { } as auxiliary block comment syntax
	doc                // It's a documentation format
	dst                // Double quotes only for strings
	end                // Everything after __END__ is treated as comment.
	eols               // No warning on EOL in string
	erlang             // $ escapes a following double quote
	f77                // Fortran 77 comment syntax
	f90                // Fortran 90 comment syntax
	hc                 // Has auxiliary hash-led comment syntax.
	ign                // Do not list as a separate language in -l or -s mode
	ktl                // Winged comments ending with newlineless EOF are OK
	lex                // Has Lex-like %% sections
	mdst               // Triple-double-quote string literals
	msst               // Triple-single-quote string literals
	mraw               // Do not interpret backslash in multistring literals
	nostr              // No string literals, or can never collide w/comments
	num                // Ignore line numbers
	she                // Permits a shebang line even with non-# comments
	solb               // Block comment start/end must begin at start of line
	solw               // Winged comment must begin at start of line
	sst                // String literals use ' not "
	texp               // Language has tilde-led regexp literals
	tick               // tick is an infix operator a well as a string quote
	xcpp               // Uses C preprocessor
	xlsbc              // Leading triple-quote string should be counted as comment
	xssep              // Uses a statement separator, not terminator
	xtsmc              // Module comments are triple-quoted strings
)

// Keep this in sync with the list of constants above!
var flagLabels = []struct {
	flag  uint
	label string
}{
	{asm, "asm"},
	{bexp, "bexp"},
	{cbs, "cbs"},
	{cnest, "cnest"},
	{crl, "crl"},
	{doc, "doc"},
	{dst, "dst"},
	{end, "end"},
	{eols, "eols"},
	{erlang, "erlang"},
	{f77, "f77"},
	{f90, "f90"},
	{hc, "hc"},
	{ign, "ign"},
	{ktl, "ktl"},
	{lex, "lex"},
	{mdst, "mdst"},
	{msst, "msst"},
	{mraw, "mraw"},
	{nostr, "nostr"},
	{num, "num"},
	{she, "she"},
	{solb, "solb"},
	{solw, "solw"},
	{sst, "sst"},
	{texp, "texp"},
	{tick, "tick"},
	{xcpp, "xcpp"},
	{xlsbc, "xlsbc"},
	{xssep, "xssep"},
	{xtsmc, "xtsmc"},
}

const assemblerLeaders = ";#*" // Intel, GAS, IBM

func init() {
	literalBrackets = []literalBracket{
		{mdst, `"""`, `"""`},
		{msst, "'''", "'''"},
	}

	var err error
	dtriple, err = regexp.Compile(dt + "." + dt)
	if err != nil {
		panic(err)
	}
	striple, err = regexp.Compile(st + "." + st)
	if err != nil {
		panic(err)
	}
	dlonely, err = regexp.Compile("^[ \t]*\"[^\"]+\"")
	if err != nil {
		panic(err)
	}
	slonely, err = regexp.Compile("^[ \t]*'[^']+'")
	if err != nil {
		panic(err)
	}
	strailer, err = regexp.Compile(".*" + st)
	if err != nil {
		panic(err)
	}
	dtrailer, err = regexp.Compile(".*" + dt)
	if err != nil {
		panic(err)
	}

	var perr error
	podheader, perr = regexp.Compile("^=[a-zA-Z]")
	if perr != nil {
		panic(perr)
	}

	neverInterestingByPrefix = []string{"."}
	actuallyInterestingByPrefix = []string{"../", "./"}
	neverInterestingByInfix = []string{".so.", "/."}
	ignoreExt := []string{"~",
		".a", ".la", ".o", ".so", ".ko",
		".gif", ".jpg", ".jpeg", ".ico", ".xpm", ".xbm", ".bmp",
		".pdf", ".eps",
		".tfm", ".ttf", ".bdf", ".afm",
		".fig", ".pic",
		".pyc", ".pyo", ".elc", ".wasm",
		".n", ".po",
		".gz", ".bz2", ".Z", ".tgz", ".zip",
		".au", ".wav", ".ogg",
	}
	neverInterestingBySuffix = make(map[string]bool)
	for i := range ignoreExt {
		neverInterestingBySuffix[ignoreExt[i]] = true
	}
	neverInterestingByBasename = map[string]bool{
		// FIXME: Should we exclude READMEs now that we're counting docs?
		"readme": true, "readme.tk": true, "readme.md": true, "readme.adoc": true,
		"changelog": true, "repository": true, "changes": true,
		"bugs": true, "todo": true, "copying": true, "maintainers": true,
		"news":      true,
		"configure": true, "autom4te.cache": true, "config.log": true,
		"config.status": true,
		"lex.yy.c":      true, "lex.yy.cc": true,
		"y.code.c": true, "y.tab.c": true, "y.tab.h": true,
		"asciidoc.js": true,
		"sccs":        true, "rcs": true,
		"waf": true, "package-lock.json": true,
	}
	cHeaderPriority = []string{"C", "C++", "Objective C"}

	generated = "GENERATED|automatically generated|generated automatically|generated by|a lexical scanner generated by flex|this is a generated file|generated with the.*utility|do not edit|do not hand-hack|DocBook XSL Stylesheets|<meta name=\"generator\" content=\"AsciiDoc|[!]PS-Adobe"

}

// Generic machinery for walking source text to count lines

// countContext is state corresponding to a single source file
type countContext struct {
	line             []byte
	lineNumber       uint // Line number in file
	charCount        uint // Character index on line
	charConsumed     uint // Characters consumed on line
	lexsection       uint // Count %% sections in a lex file
	wasPreRegexp     bool // In the plausible set for a regexp leader?
	commentLevel     int  // How many levels deep in block comments are we?
	startline        uint
	startchar        uint
	squotes          []string
	newlineLatch     bool
	lastChar         byte
	lastToken        string // Await this end-of-literal token
	startToken       string // Start token of current multline literal
	endToken         string // Expected end token of current multline literal
	slocEligible     bool   // If true this line can be counted as SLOC
	underlyingStream *os.File
	rc               *bufio.Reader
}

func (ctx *countContext) setup(path string) bool {
	var err error
	ctx.underlyingStream, err = os.Open(path)
	if err != nil {
		log.Println(err)
		return false
	}
	ctx.rc = bufio.NewReader(ctx.underlyingStream)
	ctx.lineNumber = 1
	ctx.charCount = 0
	ctx.charConsumed = 0
	ctx.slocEligible = false
	ctx.wasPreRegexp = false
	ctx.newlineLatch = false
	if debug > 2 {
		ctx.breadcrumb("All counters zeroed")
	}
	return true
}

func (ctx *countContext) teardown() {
	ctx.underlyingStream.Close()
}

func (ctx *countContext) saveMark() {
	ctx.startline = ctx.lineNumber
	ctx.startchar = ctx.charConsumed
}

func (ctx *countContext) wasNewline() bool {
	return ctx.charCount == 0 || ctx.charConsumed == 0
}

func (ctx *countContext) breadcrumb(msg string) {
	var ln uint
	var off uint
	ln = ctx.lineNumber
	off = ctx.charConsumed + 1
	fmt.Fprintf(os.Stderr, "%s:%d:%d: %s\n", ctx.underlyingStream.Name(), ln, off, msg)
}

func (ctx *countContext) markEligible() {
	if !ctx.slocEligible {
		ctx.slocEligible = true
		if debug > 1 {
			ctx.breadcrumb("marked eligible")
		}
	}
}

func (ctx *countContext) updateCounters(c byte) {
	if debug > 3 {
		ctx.breadcrumb(fmt.Sprintf("updating counters after %q", []byte{c}))
	}
	ctx.charCount++
	if c == '\n' {
		ctx.lineNumber++
		ctx.wasPreRegexp = false
		ctx.charConsumed = 0
		ctx.slocEligible = false
	} else {
		ctx.charConsumed++
		if !isspace(c) {
			ctx.wasPreRegexp = false
		}
	}
	ctx.lastChar = c
}

func (ctx *countContext) expect(exp string) bool {
	if len(exp) == 0 {
		if debug > 3 {
			ctx.breadcrumb(fmt.Sprintf("expect %q returns false.", exp))
		}
		return false
	}
	var s []byte
	var err error
	var line int
	var fn string
	if debug > 2 {
		_, fn, line, _ = runtime.Caller(1)
	}
	if s, err = ctx.rc.Peek(len(exp)); err == nil && string(s) == exp {
		ctx.rc.Discard(len(exp))
		if debug > 2 {
			ctx.breadcrumb(fmt.Sprintf("expect %q at %s:%d returns true", exp, fn, line))
		} else if transdump {
			os.Stdout.Write(s)
		}
		for i := range s {
			ctx.updateCounters(s[i])
		}
		ctx.lastToken = string(s)
		return true
	}
	if debug > 3 {
		ctx.breadcrumb(fmt.Sprintf("expect %q at %s:%d saw %q returns false", exp, fn, line, s))
	}
	return false
}

func (ctx *countContext) eof() bool {
	_, err := ctx.rc.Peek(1)
	return err == io.EOF
}

// Consume the remainder of a line, updating the line counter
func (ctx *countContext) munchline() bool {
	line, err := ctx.rc.ReadBytes('\n')
	if err == nil {
		ctx.lineNumber++
		ctx.line = line
		ctx.charCount = uint(len(line))
		ctx.charConsumed = ctx.charCount
		return true
	} else if err == io.EOF {
		return false
	} else {
		panic(err)
	}
}

// Consume the remainder of a line, updating the line counter
func (ctx *countContext) drop(excise string) bool {
	cre, err := regexp.Compile(excise)
	if err != nil {
		panic(fmt.Sprintf("unexpected failure %s while compiling %s", err, excise))
	}
	return cre.ReplaceAllLiteral(ctx.line, []byte("")) != nil
}

// matchline - does a given regexp match the last line read?
func (ctx *countContext) matchline(re string) bool {
	cre, err := regexp.Compile(re)
	if err != nil {
		panic(fmt.Sprintf("unexpected failure %s while compiling %s", err, re))
	}
	return cre.Find(ctx.line) != nil
}

func isspace(c byte) bool {
	return c == ' ' || c == '\t' || c == '\r' || c == '\n' || c == '\f'
}

func isAlphameric(c byte) bool {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9')
}

// Verifier functions for checking that files with disputed extensions
// are actually of the types we think they are.

// reallyMATLAB - returns true if filename contents really are MATLAB.
// We have to disambiguate against MUMPS, Objective-C, and mm
func reallyMATLAB(ctx *countContext, path string, ext string) bool {
	// Because these can be unabiguously identified, we
	// don't set a latch here.
	return hasKeywords(ctx, path, "MATLAB", []string{"^%{", "^%", "^end"})
}

// reallyObjectiveC - returns true if filename contents really are objective-C.
// We have to disambiguate against MUMPS, MATAB, and mm
func reallyObjectiveC(ctx *countContext, path string, ext string) bool {
	special := false // Did we find a special Objective-C pattern?
	isObjC := false  // Value to determine.
	braceLines := 0  // Lines that begin/end with curly braces.
	plusMinus := 0   // Lines that begin with + or -.
	wordMain := 0    // Did we find "main("?

	ctx.setup(path)
	defer ctx.teardown()

	for ctx.munchline() {
		if ctx.matchline("^#import") {
			return true
		}
		if ctx.matchline("^\\s*[{}]") || ctx.matchline("[{}];?\\s*") {
			braceLines++
		}
		if ctx.matchline("^\\s*[+-]") {
			plusMinus++
		}
		if ctx.matchline("\\bmain\\s*\\(") { // "main" followed by "("?
			wordMain++
		}
		// Handle /usr/src/redhat/BUILD/egcs-1.1.2/gcc/objc/linking.m:
		if ctx.matchline("(?i)^\\s*\\[object name\\];\\s*") {
			special = true
		}

		if (braceLines > 1) && ((plusMinus > 1) || wordMain > 0 || special) {
			isObjC = true
		}

	}

	if debug > 0 {
		fmt.Fprintf(os.Stderr, "objc verifier returned %t on %s\n", isObjC, path)
	}

	return isObjC
}

func hasKeywords(ctx *countContext, path string, lang string, tells []string) bool {
	matching := false // Value to determine.

	ctx.setup(path)
	defer ctx.teardown()

	for ctx.munchline() {
		for i := range tells {
			if ctx.matchline(tells[i]) {
				matching = true
				break
			}
		}
	}

	if debug > 0 {
		fmt.Fprintf(os.Stderr, "%s verifier returned %t on %s\n",
			lang, matching, path)
	}

	return matching
}

// reallyCsharp - returns true if filename contents really are C#.
func reallyCsharp(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "C#", []string{"//", "/*"})
}

// reallyMetafont - returns true if filename contents really are Turing.
func reallyMetafont(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "Metafont", []string{"%"})
}

// reallyTuring - returns true if filename contents really are Turing.
func reallyTuring(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "Turing", []string{"function"})
}

// reallyFsharp - returns true if filename contents really are Fsharp.
func reallyFsharp(ctx *countContext, path string, ext string) bool {
	// Resolve a collision with FORTH, which collides with only one
	// of F#'s extensions.
	if ext == ".fs" {
		return hasKeywords(ctx, path, "F#", []string{"//", "module"})
	}
	return true
}

// reallyOccam - returns true if filename contents really are occam.
func reallyOccam(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "occam", []string{"--", "PROC"})
}

// reallyLex - returns true if filename contents really are lex.
func reallyLex(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "Lex", []string{"%{", "%%", "%}"})
}

// reallyBCPL - returns true if filename contents really are BCPL.
func reallyBCPL(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "BCPL", []string{"LET"})
}

// reallyPOP11 - returns true if filename contents really are POP11.
func reallyPOP11(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "pop11", []string{"define", "printf"})
}

// reallySather - returns true if filename contents really are sather.
func reallySather(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "Sather", []string{"class"})
}

// reallyRebol - returns true if filename contents really are Rebol.
func reallyRebol(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "Rebol", []string{"REBOL"})
}

// reallyProlog - returns true if filename contents really are Prolog.
// Without this check, Perl files will be falsely identified.
func reallyProlog(ctx *countContext, path string, ext string) bool {
	if ext != ".pl" {
		return true
	}
	ctx.setup(path)
	defer ctx.teardown()

	for ctx.munchline() {
		if ctx.matchline("^use") {
			return false // Must be Perl
		}
		if bytes.HasPrefix(ctx.line, []byte("#")) {
			return false
		} else if ctx.matchline("\\$[[:alpha]]") {
			return false
		}
	}

	return true
}

// reallyWish - returns true if filename contents really are Wish/Tcl.
func reallyWish(ctx *countContext, path string, ext string) bool {
	return hasKeywords(ctx, path, "wish", []string{"wish"})
}

// reallyExpect - filename, returns true if its contents really are Expect.
//
// dwheeler had this to say:
//
// Many "exp" files (such as in Apache and Mesa) are just "export" data,
// summarizing something else (e.g., its interface).
// Sometimes (like in RPM) it's just misc. data.
// Thus, we need to look at the file to determine
// if it's really an "expect" file.
// The heuristic is as follows: it's Expect _IF_ it:
// 1. has "load_lib" command and either "#" comments or {}.
// 2. {, }, and one of: proc, if, [...], expect
func reallyExpect(ctx *countContext, path string, ext string) bool {
	var isExpect = false // Value to determine.

	var beginBrace bool // Lines that begin with curly braces.
	var endBrace bool   // Lines that begin with curly braces.
	var loadLib bool    // Lines with the LoadLib command.
	var foundProc bool
	var foundIf bool
	var foundBrackets bool
	var foundExpect bool
	var foundPound bool

	ctx.setup(path)
	defer ctx.teardown()

	for ctx.munchline() {
		if ctx.matchline("#") {
			foundPound = true
			// Delete trailing comments
			i := bytes.Index(ctx.line, []byte("#"))
			if i > -1 {
				ctx.line = ctx.line[:i]
			}
		}

		if ctx.matchline("^\\s*\\{") {
			beginBrace = true
		}
		if ctx.matchline("\\{\\s*$") {
			beginBrace = true
		}
		if ctx.matchline("^\\s*}") {
			endBrace = true
		}
		if ctx.matchline("};?\\s*$") {
			endBrace = true
		}
		if ctx.matchline("^\\s*loadLib\\s+\\S") {
			loadLib = true
		}
		if ctx.matchline("^\\s*proc\\s") {
			foundProc = true
		}
		if ctx.matchline("^\\s*if\\s") {
			foundIf = true
		}
		if ctx.matchline("\\[.*\\]") {
			foundBrackets = true
		}
		if ctx.matchline("^\\s*expect\\s") {
			foundExpect = true
		}
	}

	if loadLib && (foundPound || (beginBrace && endBrace)) {
		isExpect = true
	}
	if beginBrace && endBrace &&
		(foundProc || foundIf || foundBrackets || foundExpect) {
		isExpect = true
	}

	if debug > 0 {
		fmt.Fprintf(os.Stderr, "Expect verifier returned %t on %s\n", isExpect, path)
	}

	return isExpect
}

// reallyPascal - returns  true if filename contents really are Pascal.
func reallyPascal(ctx *countContext, path string, ext string) bool {
	if ext == ".pas" {
		return true
	}
	// This fast check depends on the assumption that we'll never call this
	// verifier against Modula/Oberon/ML - reasonable as nothing with
	// any of the file extensions for those languages should ever get here.
	// Was originally put in to avoid false-matching pop11.
	if hasKeywords(ctx, path, "Pascal", []string{`[(][*]`}) {
		return true
	}
	//
	// dwheeler had this to say:
	//
	// This isn't as obvious as it seems.
	// Many ".p" files are Perl files
	// (such as /usr/src/redhat/BUILD/ispell-3.1/dicts/czech/glob.p),
	// others are C extractions
	// (such as /usr/src/redhat/BUILD/linux/include/linux/umsdos_fs.p
	// and some files in linuxconf).
	// However, test files in "p2c" really are Pascal, for example.
	//
	// Note that /usr/src/redhat/BUILD/ucd-snmp-4.1.1/ov/bitmaps/UCD.20.p
	// is actually C code.  The heuristics determine that they're not Pascal,
	// but because it ends in ".p" it's not counted as C code either.
	// I believe this is actually correct behavior, because frankly it
	// looks like it's automatically generated (it's a bitmap expressed as code).
	// Rather than guess otherwise, we don't include it in a list of
	// source files.  Let's face it, someone who creates C files ending in ".p"
	// and expects them to be counted by default as C files in SLOCCount needs
	// their head examined.  I suggest examining their head
	// with a sucker rod (see syslogd(8) for more on sucker rods).
	//
	// This heuristic counts as Pascal such files such as:
	//  /usr/src/redhat/BUILD/teTeX-1.0/texk/web2c/tangleboot.p
	// Which is hand-generated.  We don't count woven documents now anyway,
	// so this is justifiable.

	// The heuristic is as follows: it's Pascal _IF_ it has all of the following
	// (ignoring {...} and (*...*) comments):
	// 1. "^..program NAME" or "^..unit NAME",
	// 2. "procedure", "function", "^..interface", or "^..implementation",
	// 3. a "begin", and
	// 4. it ends with "end.",
	//
	// Or it has all of the following:
	// 1. "^..module NAME" and
	// 2. it ends with "end.".
	//
	// Or it has all of the following:
	// 1. "^..program NAME",
	// 2. a "begin", and
	// 3. it ends with "end.".
	//
	// The "end." requirements in particular filter out non-Pascal.
	//
	// Note (jgb): this does not detect Pascal main files in fpc, like
	// fpc-1.0.4/api/test/testterminfo.pas, which does not have "program" in
	// it
	var isPascal bool // Value to determine.

	var hasProgram bool
	var hasUnit bool
	var hasModule bool
	var hasProcedureOrFunction bool
	var hasBegin bool
	var foundTerminatingEnd bool

	ctx.setup(path)
	defer ctx.teardown()

	for ctx.munchline() {
		// Ignore {...} comments on this line; imperfect, but effective.
		ctx.drop("\\{.*?\\}")
		// Ignore (*...*) comments on this line; imperfect but effective.
		ctx.drop("\\(\\*.*\\*\\)")

		if ctx.matchline("(?i)\\bprogram\\s+[A-Za-z]") {
			hasProgram = true
		}
		if ctx.matchline("(?i)\\bunit\\s+[A-Za-z]") {
			hasUnit = true
		}
		if ctx.matchline("(?i)\\bmodule\\s+[A-Za-z]") {
			hasModule = true
		}
		if ctx.matchline("(?i)\\bprocedure\\b") {
			hasProcedureOrFunction = true
		}
		if ctx.matchline("(?i)\\bfunction\\b") {
			hasProcedureOrFunction = true
		}
		if ctx.matchline("(?i)^\\s*interface\\s+") {
			hasProcedureOrFunction = true
		}
		if ctx.matchline("(?i)^\\s*implementation\\s+") {
			hasProcedureOrFunction = true
		}
		if ctx.matchline("(?i)\\bbegin\\b") {
			hasBegin = true
		}
		// Originally dw said: "This heuristic fails if there
		// are multi-line comments after "end."; I haven't
		// seen that in real Pascal programs:"
		// But jgb found there are a good quantity of them in
		// Debian, specially in fpc (at the end of a lot of
		// files there is a multiline comment with the
		// changelog for the file).  Therefore, assume Pascal
		// if "end." appears anywhere in the file.
		if ctx.matchline("(?i)end\\.\\s*$") {
			foundTerminatingEnd = true
		}
	}

	// Okay, we've examined the entire file looking for clues;
	// let's use those clues to determine if it's really Pascal:
	isPascal = (((hasUnit || hasProgram) && hasProcedureOrFunction &&
		hasBegin && foundTerminatingEnd) ||
		(hasModule && foundTerminatingEnd) ||
		(hasProgram && hasBegin && foundTerminatingEnd))

	if debug > 0 {
		fmt.Fprintf(os.Stderr, "Pascal verifier returned %t on %s\n", isPascal, path)
	}

	return isPascal
}

func wasGeneratedAutomatically(ctx *countContext, path string, eolcomment string) bool {
	// Determine if the file was generated automatically.
	// Use a simple heuristic: check if first few lines have phrases like
	// "generated automatically", "automatically generated", "Generated by",
	// or "do not edit" as the first
	// words in the line (after possible comment markers and spaces).
	i := 15 // Look at first 15 lines.
	ctx.setup(path)
	defer ctx.teardown()

	// Avoid blowing up if the comment leader is "*" (as in COBOL)
	// or "\" as in ABC.
	if eolcomment == "*" || eolcomment == "\\" {
		eolcomment = ""
	} else {
		eolcomment = "|" + eolcomment
	}
	re := "(\\*" + eolcomment + ").*(?i:" + generated + ")"
	cre, err := regexp.Compile(re)
	if err != nil {
		panic(fmt.Sprintf("unexpected failure while building %s", re))
	}

	for ctx.munchline() && i > 0 {
		//fmt.Fprintf(os.Stderr, "Matching %s against %s", ctx.line, re)
		if cre.Find(ctx.line) != nil {
			if debug > 0 {
				fmt.Fprintf(os.Stderr, "%s: is generated\n", path)
			}
			return true
		}
		i--
	}

	return false
}

// hashbang - hunt for a specified string in the first line of an executable
func hashbang(ctx *countContext, path string, interpreters []string) bool {
	fi, err := os.Stat(path)
	// If it's not executable by somebody, don't read for hashbang
	if err != nil || (fi.Mode()&01111) == 0 {
		return false
	}
	ctx.setup(path)
	defer ctx.teardown()
	s, err := ctx.rc.ReadString('\n')
	if err != nil || !strings.HasPrefix(s, "#!") {
		return false
	}
	s = strings.TrimSpace(s)
	if debug > 0 {
		fmt.Fprintf(os.Stderr, "hashbang checking %q against alternatives %s\n", s, interpreters)
	}
	for _, alternative := range interpreters {
		// First part of this guard prevents R from making mischief
		if len(alternative) >= 2 && strings.HasSuffix(s, alternative) {
			return true
		}
	}
	return false
}

func dumpTransition(legend string) {
	os.Stdout.WriteString("\x1B[7m" + legend + "\x1B[0m")
}

// genericCounter - Count SLOC (and possibly LLOC) in a source file for a generic procedral language
func genericCounter(ctx *countContext, path string, syntax genericLanguage, ext string) []SourceStat {
	const stateNORMAL = 0        // in running text
	const stateINSTRING = 1      // in single-line string
	const stateINMULTISTRING = 2 // in multi-line string
	const stateBLOCKCOMMENT = 3  // in block comment
	const stateWINGEDCOMMENT = 4 // in winged comment
	const stateREGEXP = 5        // in regexp literal
	var stateNames = [6]string{"NORMAL", "STRING",
		"MULTISTRING", "BLOCKCOMMENT", "WINGEDCOMMENT", "REGEXP"}

	var stats SourceStat
	mode := stateNORMAL

	if syntax.verifier != nil && !syntax.verifier(ctx, path, ext) {
		return []SourceStat{stats}
	}

	ctx.setup(path)
	defer ctx.teardown()

	stats.Path = path
	stats.Language = syntax.Name
	stats.markup = syntax.property(doc)

	wingedComment := func(syntax genericLanguage) bool {
		if syntax.property(f77) || syntax.property(f90) {
			if ctx.expect("!") {
				return !ctx.expect("hpf$") && !ctx.expect("omp$")
			} else if ctx.wasNewline() && syntax.property(f77) {
				if ctx.expect("c") || ctx.expect("*") || ctx.expect("!") {
					return !ctx.expect("hpf$") && !ctx.expect("omp$")
				}
			}
			return false
		}
		if syntax.Wcl == "" {
			return false
		}
		if syntax.property(solw) && !ctx.wasNewline() {
			return false
		}
		if ctx.expect(syntax.Wcl) {
			return true
		}
		if syntax.property(hc) && ctx.expect("#") {
			return true
		}
		if syntax.property(asm) {
			for i := range assemblerLeaders {
				if ctx.expect(string([]byte{assemblerLeaders[i]})) {
					return true
				}
			}
		}
		return false
	}

	multiString := func(syntax genericLanguage) bool {
		if debug > 2 {
			ctx.breadcrumb("multistring check")
		}
		for _, entry := range literalBrackets {
			if syntax.property(entry.quirk) && ctx.expect(entry.start) {
				ctx.startToken = entry.start
				ctx.endToken = entry.end
				return true
			}
		}
		for _, e := range syntax.Ml {
			if ctx.expect(e[0]) {
				ctx.startToken = e[0]
				ctx.endToken = e[1]
				return true
			}
		}
		return false
	}

	blockCommentStart := func(syntax genericLanguage) bool {
		if syntax.property(solb) && !ctx.wasNewline() {
			return false
		}
		if ctx.expect(syntax.Bcl) {
			return true
		} else if syntax.property(crl) && ctx.expect("{") {
			return true
		}
		return false
	}

	blockCommentEnd := func(syntax genericLanguage) bool {
		if syntax.property(solb) && !ctx.wasNewline() {
			return false
		}
		if ctx.expect(syntax.Bct) {
			return true
		} else if syntax.property(crl) && ctx.expect("}") {
			return true
		}
		return false
	}

	inlineString := func() bool {
		var st string
		if syntax.property(nostr) {
			return false
		}
		if !syntax.property(dst) && ctx.expect("'") {
			st = "'"
		} else if !syntax.property(sst) && ctx.expect("\"") {
			st = "\""
		}
		if st == "" {
			return false
		}
		// We know we've seen a string delimiter
		ctx.squotes = append(ctx.squotes, st)
		return true
	}

	eol := func() bool {
		return ctx.expect("\n")
	}

	for !ctx.eof() {
		if debug > 2 {
			ctx.breadcrumb("top of loop")
		}
		// backslash disables whatever state change
		// the following character would have triggered
		/// FIXME: Should this set slocEligible?  Edge case...
		if syntax.property(cbs) && ctx.expect("\\") && !(mode == stateINMULTISTRING && syntax.property(mraw)) {
			if debug > 1 {
				ctx.breadcrumb("escaping...")
			}
			if ctx.expect("\n") {
				if debug > 1 {
					ctx.breadcrumb("backslash SLOC++")
				} else if transdump {
					dumpTransition("+")
				}
				stats.SLOC++
			} else {
				if debug > 1 {
					s, _ := ctx.rc.Peek(1)
					ctx.breadcrumb(fmt.Sprintf("discarding %q", s))
				}
				ctx.rc.Discard(1)
			}
			continue
		}

		if mode == stateNORMAL {
			if ctx.expect(syntax.St) {
				ctx.markEligible()
				stats.LLOC++
				if debug > 1 {
					ctx.breadcrumb("lloc++")
				}
				continue
			} else if syntax.property(tick) && ctx.expect("'") {
				ctx.markEligible()
				s, _ := ctx.rc.Peek(2)
				if len(s) == 2 && s[1] == '\'' {
					if debug > 1 {
						ctx.breadcrumb("character literal")
					} else if transdump {
						os.Stdout.Write(s)
					}
					ctx.updateCounters(s[0])
					ctx.updateCounters(s[1])
					ctx.rc.Discard(2)
					continue
				}
				if debug > 1 {
					ctx.breadcrumb("tick operator")
				}
				continue
			} else if syntax.property(she) && ctx.charCount == 0 && ctx.expect("#!") {
				/* we *don't* set slocEligible here! */
				if debug > 1 {
					ctx.breadcrumb("hashbang leader")
				}
				mode = stateWINGEDCOMMENT
				ctx.saveMark()
				continue

			} else if syntax.property(lex) && ctx.wasNewline() && ctx.expect("%%\n") {
				ctx.lexsection++
				if debug > 1 {
					ctx.breadcrumb(fmt.Sprintf("lexsection = %d\n", ctx.lexsection))
				}
				continue
			} else if ctx.lexsection != 1 && multiString(syntax) {
				ctx.markEligible()
				if debug > 1 {
					ctx.breadcrumb(fmt.Sprintf("%q triggers NORMAL -> MULTISTRING\n", ctx.startToken))
				} else if transdump {
					dumpTransition("MULTISTRING")
				}
				mode = stateINMULTISTRING
				ctx.saveMark()
				continue
			} else if ctx.lexsection != 1 && inlineString() {
				ctx.markEligible()
				if debug > 1 {
					ctx.breadcrumb(fmt.Sprintf("%q triggers NORMAL -> STRING", ctx.squotes[len(ctx.squotes)-1]))
				} else if transdump {
					dumpTransition("STRING")
				}

				mode = stateINSTRING
				ctx.saveMark()
				continue
			} else if ctx.lexsection != 1 && blockCommentStart(syntax) {
				/* we *don't* set slocEligible here! */
				if debug > 1 {
					ctx.breadcrumb(fmt.Sprintf("%q triggers NORMAL -> BLOCKCOMMENT", syntax.Bcl))
				} else if transdump {
					dumpTransition("BLOCKCOMMENT")
				}

				ctx.commentLevel++
				if debug > 1 {
					ctx.breadcrumb(fmt.Sprintf("comment level incremented to %d", ctx.commentLevel))
				}
				mode = stateBLOCKCOMMENT
				ctx.saveMark()
				continue
			} else if wingedComment(syntax) {
				/* we *don't* set slocEligible here! */
				if debug > 1 {
					ctx.breadcrumb("saw winged-comment leader " + syntax.Wcl)
				} else if transdump {
					dumpTransition("WINGEDCOMMENT")
				}
				mode = stateWINGEDCOMMENT
				ctx.saveMark()
				continue
			} else if syntax.property(bexp) && ctx.expect("/") {
				ctx.markEligible()
				if debug > 1 {
					ctx.breadcrumb("saw bare-regexp leader")
				}
				// Ugh. The problem here is that in languages with
				// the bexp quirk  there is no reliable way to tell
				// without a full parse whether / in running text
				// is a leader for a regexp or a division sign - and
				// if it's a regexp literal it could contain a comment
				// start.  So we treat the following text like a
				// winged comment, hoping there's no block comment
				// start after the literal.
				mode = stateWINGEDCOMMENT
				continue
			} else if syntax.property(end) && ctx.expect("__END__\n") {
				break
			} else if syntax.property(erlang) && ctx.expect("$\"") {
				continue
			} else if syntax.property(texp) && ctx.expect("~/") {
				ctx.markEligible()
				if debug > 1 {
					ctx.breadcrumb("NORMAL->REGEXP\n")
				} else if transdump {
					dumpTransition("REGEXP")
				}

				mode = stateREGEXP
				ctx.wasPreRegexp = false
				continue
			}
		} else if mode == stateREGEXP {
			if ctx.expect("/") {
				if debug > 1 {
					ctx.breadcrumb("REGEXP->NORMAL\n")
				} else if transdump {
					dumpTransition("NORMAL")
				}
				mode = stateNORMAL
			}
		} else if mode == stateINMULTISTRING {
			if ctx.expect(ctx.endToken) {
				ctx.markEligible()
				if debug > 1 {
					ctx.breadcrumb(fmt.Sprintf("%q triggers MULTISTRING->NORMAL", ctx.endToken))
				} else if transdump {
					dumpTransition("NORMAL")
				}
				ctx.endToken = ""
				mode = stateNORMAL
				continue
			}
		} else if mode == stateINSTRING {
			countline := ctx.slocEligible
			if inlineString() {
				ctx.markEligible()
				// We just pushed the quote that got us here.
				// Check to see if it matches start of stack.
				// if so, drop to normal mode.
				if ctx.squotes[len(ctx.squotes)-1] == ctx.squotes[0] {
					if debug > 1 {
						ctx.breadcrumb(fmt.Sprintf("%q triggers STRING -> NORMAL", ctx.squotes[len(ctx.squotes)-1]))
					} else if transdump {
						dumpTransition("NORMAL")
					}
					mode = stateNORMAL
					ctx.squotes = ctx.squotes[:0]
					ctx.newlineLatch = false
				}
				continue
			} else if eol() {
				if countline {
					if debug > 1 {
						ctx.breadcrumb("string SLOC++")
					} else if transdump {
						dumpTransition("+")
					}
					stats.SLOC++
				}
				// We found a bare newline in a string without
				// preceding backslash.
				if !syntax.property(eols) && !ctx.newlineLatch {
					ctx.breadcrumb("WARNING - newline in string")
					ctx.newlineLatch = true
				}

				// We COULD warn & reset mode to
				// "Normal", but lots of code does this,
				// so we'll just depend on the warning
				// for ending the program in a string to
				// catch syntactically erroneous
				// programs.
				continue
			}
		} else { /* stateBLOCKCOMMENT or stateWINGEDCOMMENT */
			countLine := ctx.slocEligible
			if eol() {
				if mode == stateWINGEDCOMMENT {
					if debug > 1 {
						ctx.breadcrumb(fmt.Sprintf("end of winged comment triggers WINGEDCOMMENT->NORMAL"))
					} else if transdump {
						dumpTransition("NORMAL")
					}
					mode = stateNORMAL
					if countLine {
						if debug > 1 {
							ctx.breadcrumb("SLOC++")
						} else if transdump {
							dumpTransition("+")
						}
						stats.SLOC++
					}
				}
				continue
			}

			if mode == stateBLOCKCOMMENT {
				if blockCommentEnd(syntax) {
					// Pascal {} comments never nest
					if ctx.lastToken == "}" && ctx.commentLevel != 1 {
						continue
					}
					ctx.commentLevel--
					if debug > 1 {
						ctx.breadcrumb(fmt.Sprintf("comment level decremented to %d", ctx.commentLevel))
					}
					if !syntax.property(cnest) || ctx.commentLevel == 0 {
						if debug > 1 {
							ctx.breadcrumb(fmt.Sprintf("%s triggers BLOCKCOMMENT->NORMAL", syntax.Bct))
						} else if transdump {
							dumpTransition("NORMAL")
						}
						mode = stateNORMAL
					}
					continue
				} else if blockCommentStart(syntax) {
					// Pascal {} comments never nest
					if ctx.lastToken == "{" {
						continue
					}
					ctx.commentLevel++
					if debug > 1 {
						ctx.breadcrumb(fmt.Sprintf("comment level incremented to %d", ctx.commentLevel))
					}
					continue
				}
			}
		}

		countLine := ctx.slocEligible
		c, err := ctx.rc.ReadByte()
		if err != nil && err != io.EOF {
			panic("error while reading a character")
		}
		if debug > 1 {
			ctx.breadcrumb(fmt.Sprintf("state %s advancing on %q", stateNames[mode], []byte{c}))
		} else if transdump {
			os.Stdout.Write([]byte{c})
		}
		ctx.updateCounters(c)
		if !isspace(c) && (mode == stateNORMAL || mode == stateINSTRING || mode == stateINMULTISTRING) {
			if syntax.property(num) && strings.IndexByte("0123456789", c) > -1 {
				continue
			}
			ctx.markEligible()
		}
		if mode == stateNORMAL {
			if strings.IndexByte("~=", c) > -1 {
				// ~ in awk/Haxe, = in anywhere
				ctx.wasPreRegexp = true
			} else if !isspace(c) {
				ctx.wasPreRegexp = false
			}
		}
		if c == '\n' && countLine {
			if debug > 1 {
				ctx.breadcrumb("SLOC++")
			} else if transdump {
				dumpTransition("+")
			}
			stats.SLOC++
		}
	}
	/* We're done with the file.  Handle EOF-without-EOL. */
	if ctx.slocEligible {
		if debug > 1 {
			ctx.breadcrumb("widow line SLOC++")
		} else if transdump {
			dumpTransition("+")
		}
		stats.SLOC++
	}

	if mode == stateBLOCKCOMMENT || mode == stateWINGEDCOMMENT {
		var ctype string
		if mode == stateWINGEDCOMMENT {
			ctype = "trailing"
		} else {
			ctype = "block"
		}
		// The reason for this ugly guard is JavaScript without line breaks
		// embedded in HTML. The bare regexp element looks like a winged
		// comment. We could make this unnecessary by adding a new state.
		if !syntax.property(ktl) && (!syntax.property(bexp) || ctx.lineNumber > 1) {
			ctx.breadcrumb(fmt.Sprintf("ERROR - terminated in %s comment beginning %d:%d", ctype, ctx.startline, ctx.startchar))
		}
	} else if mode == stateINSTRING {
		ctx.breadcrumb(fmt.Sprintf("ERROR - terminated in string beginning %d:%d", ctx.startline, ctx.startchar))
	}

	if syntax.property(num) {
		stats.LLOC = stats.SLOC
	}

	return []SourceStat{stats}
}

func goCounter(path string) uint {
	var lloc uint

	content, err1 := ioutil.ReadFile(path)
	if err1 != nil {
		return 0
	}

	fset := token.NewFileSet() // positions are relative to fset
	f, err2 := parser.ParseFile(fset, path, content, 0)
	if err2 != nil {
		return 0
	}

	// Inspect the AST and print all identifiers and literals.
	ast.Inspect(f, func(n ast.Node) bool {
		switch n.(type) {
		case *ast.AssignStmt: // sssignment or short variable declaration
			lloc++
		case *ast.BranchStmt: // break, continue, goto, or fallthrough
			lloc++
		case *ast.DeclStmt: // declaration in a statement list.
			lloc++
		case *ast.DeferStmt: // a defer statement.
			lloc++
		case *ast.ExprStmt: // stand-alone expression in a statement list.
			lloc++
		case *ast.GenDecl: // an import, constant, type or variable declaration
			lloc++
		case *ast.GoStmt: // go xxxx
			lloc++
		//case *ast.IfStmt:	// an if statement
		//	lloc++
		case *ast.ImportSpec: // package import line
			lloc++
		case *ast.IncDecStmt: // incement or decrement statement
			lloc++
		//case *ast.RangeStmt:	// for statement with a range clause.
		//	lloc++
		case *ast.ReturnStmt: // a return statement.
			lloc++
		//case *ast.SelectStmt:	// a select statement.
		//	lloc++
		case *ast.SendStmt: // a send statement.
			lloc++
			//case *ast.SwitchStmt:	// a switch statement.
			//	lloc++
		}
		// Not counted: BlockStmt, FuncDecl
		// Including IfStmt, RangeStmt, SelectStmt, SwitchStmt
		// is probably a better complexity metric, but no longer
		// strictly comparable with counting semis in C.
		return true
	})
	return lloc
}

// perlCounter - count SLOC in Perl
//
// Physical lines of Perl are MUCH HARDER to count than you'd think.
// Comments begin with "#".
// Also, anything in a "perlpod" is a comment.
// See perlpod(1) for more info; a perlpod starts with
// \s*=command, can have more commands, and ends with \s*=cut.
// Note that = followed by space is NOT a perlpod.
// Although we ignore everything after __END__ in a file,
// we will count everything after __DATA__; there's arguments for counting
// and for not counting __DATA__.
//
// What's worse, "here" documents must be COUNTED AS CODE, even if
// they're FORMATTED AS A PERLPOD.  Surely no one would do this, right?
// Sigh... it can happen. See perl5.005_03/pod/splitpod.
func perlCounter(ctx *countContext, path string) SourceStat {
	var heredoc string
	var isinpod bool
	var stats SourceStat

	ctx.setup(path)
	stats.Path = path
	defer ctx.teardown()

	for ctx.munchline() {
		// Delete trailing comments
		i := bytes.Index(ctx.line, []byte("#"))
		if i > -1 {
			ctx.line = ctx.line[:i]
		}

		ctx.line = bytes.Trim(ctx.line, " \t\r\n")

		if heredoc != "" && strings.HasPrefix(string(ctx.line), heredoc) {
			heredoc = "" //finished here doc.
		} else if i := bytes.Index(ctx.line, []byte("<<")); i > -1 {
			// Beginning of a here document.
			heredoc = string(bytes.Trim(ctx.line[i:], "< \t\"';,"))
		} else if len(heredoc) == 0 && bytes.HasPrefix(ctx.line, []byte("=cut")) {
			// Ending a POD?
			if !isinpod {
				fmt.Fprintf(os.Stderr, "%q, %d: cut without pod start\n",
					path, ctx.lineNumber)
			}
			isinpod = false
			continue // Don't count the cut command.
		} else if len(heredoc) == 0 && podheader.Match(ctx.line) {
			// Starting or continuing a POD?
			// Perlpods can have multiple contents, so
			// it's okay if isinpod == true.  Note that
			// =(space) isn't a POD; library file
			// perl5db.pl does this!
			isinpod = true
		} else if bytes.HasPrefix(ctx.line, []byte("__END__")) {
			// Stop processing this file on __END__.
			break
		}
		if !isinpod && len(ctx.line) > 0 {
			stats.SLOC++
			// Perl is actually xssep, so this undercounts a bit.
			if strings.Contains(string(ctx.line), ";") {
				stats.LLOC++
			}
		}
	}

	return stats
}

// countGeneric - recognize lots of languages with generic syntax
func countGeneric(path string) []SourceStat {
	ctx := new(countContext)
	var singleStat SourceStat
	singleStat.Path = path

	autofilter := func(eolcomment string) bool {
		if wasGeneratedAutomatically(ctx, path, eolcomment) {
			if listignores {
				fmt.Println(path)
				return true
			}
			if debug > 0 {
				fmt.Printf("automatic generation filter failed: %s\n", path)
			}
			return true
		}
		if debug > 0 {
			fmt.Printf("automatic generation filter passed: %s\n", path)
		}
		return false
	}

	for i := range genericLanguages {
		lang := genericLanguages[i]
		lopath := strings.ToLower(path)
		var trigger string
		if (lang.Wcl == "#" || lang.property(she)) && hashbang(ctx, path, lang.matchables()) {
			trigger = "#"
		} else {
			trigger = lang.suffixMatches(lopath)
		}
		if trigger != "" {
			if autofilter(lang.Wcl) {
				return []SourceStat{singleStat}
			}
			stats := genericCounter(ctx, path, lang, trigger)
			if strings.HasSuffix(path, ".go") {
				stats[0].LLOC = goCounter(path)
			}
			if stats[0].nonEmpty() {
				return stats
			}
		}
	}

	if strings.HasSuffix(path, ".pl") || strings.HasSuffix(path, ".pm") || strings.HasSuffix(path, ".ph") || hashbang(ctx, path, []string{"perl"}) {
		if autofilter("#") {
			return []SourceStat{singleStat}
		}
		singleStat = perlCounter(ctx, path)
		singleStat.Language = "Perl"
		if hasKeywords(ctx, path, "Perl6", []string{"use v6;"}) {
			singleStat.Language = "Perl6"
		}
		return []SourceStat{singleStat}
	}

	// Without this fallthrough to returning an empty stat block,
	// we'd get no report on unclassifiables.
	return []SourceStat{singleStat}
}

func isDirectory(path string) bool {
	fileInfo, err := os.Stat(path)
	return err == nil && fileInfo.Mode().IsDir()
}

func isRegular(path string) bool {
	fileInfo, err := os.Stat(path)
	return err == nil && fileInfo.Mode().IsRegular()
}

// filter - winnows out uninteresting paths before handing them to process
func filter(path string, info os.FileInfo, err error) error {
	if debug > 0 {
		fmt.Printf("entering filter: %s\n", path)
	}
	suffix := filepath.Ext(path)
	if suffix != "" && neverInterestingBySuffix[suffix] {
		if debug > 0 {
			fmt.Printf("suffix filter failed: %s\n", path)
		}
		return err
	}
	for i := range neverInterestingByPrefix {
		var interesting bool = false
		if strings.HasPrefix(path, neverInterestingByPrefix[i]) {
			for j := range actuallyInterestingByPrefix {
				if strings.HasPrefix(path, actuallyInterestingByPrefix[j]) {
					interesting = true
					break
				}
			}
			if !interesting {
				if debug > 0 {
					fmt.Printf("prefix filter failed: %s\n", path)
				}
				return err
			}
		}
	}
	for i := range neverInterestingByInfix {
		if strings.Contains(path, neverInterestingByInfix[i]) {
			if debug > 0 {
				fmt.Printf("infix filter failed: %s\n", path)
			}
			if isDirectory(path) {
				if debug > 0 {
					fmt.Printf("directory skipped: %s\n", path)
				}
				return filepath.SkipDir
			}
			return err
		}
	}
	basename := filepath.Base(path)
	if neverInterestingByBasename[strings.ToLower(basename)] {
		if debug > 0 {
			fmt.Printf("basename filter failed: %s\n", path)
		}
		return err
	}
	if exclusions != nil && exclusions.MatchString(path) {
		if debug > 0 {
			fmt.Printf("exclusion '%s' filter failed: %s\n", exclusions, path)
		}
		return err
	}

	/* has to come after the infix check for directory */
	if !isRegular(path) {
		if debug > 0 {
			fmt.Printf("regular-file filter failed: %s\n", path)
		}
		return err
	}

	/* toss generated Makefiles */
	if basename == "Makefile" {
		if _, err := os.Stat(path + ".in"); err == nil {
			if debug > 0 {
				fmt.Printf("generated-makefile filter failed: %s\n", path)
			}
			return err
		}
	}

	/* ignore an installed copy of waf and any waf build directory */
	if basename == "build" && isDirectory("build") && isRegular("waf") {
		return err
	}

	if debug > 0 {
		fmt.Printf("passed filter: %s\n", path)
	}

	// Now the real work gets done
	for _, st := range countGeneric(path) {
		pipeline <- st
	}

	return err
}

type countRecord struct {
	language   string
	slinecount uint
	llinecount uint
	filecount  uint
}

func cocomo81(sloc uint) float64 {
	const cTIMEMULT = 2.4
	const cTIMEEXP = 1.05
	fmt.Printf("\nTotal Physical Source Lines of Code (SLOC)                = %d\n", sloc)
	fmt.Printf(" (COCOMO I model, Person-Months = %2.2f * (KSLOC**%2.2f))\n", cTIMEMULT, cTIMEEXP)
	return cTIMEMULT * math.Pow(float64(sloc)/1000, cTIMEEXP)
}

// See https://en.wikipedia.org/wiki/COCOMO
func cocomo2000(lloc uint) float64 {
	const cTIMEMULT = 3.2
	const cTIMEEXP = 1.05
	fmt.Printf("\nTotal Logical Source Lines of Code (LLOC)                 = %d\n", lloc)
	fmt.Printf(" (COCOMO II model, Person-Months = %2.2f * (KLOC**%2.2f))\n", cTIMEMULT, cTIMEEXP)
	return cTIMEMULT * math.Pow(float64(lloc)/1000, cTIMEEXP)
}

func reportCocomo(loc uint, curve func(uint) float64) {
	const cSCHEDMULT = 2.5
	const cSCHEDEXP = 0.38
	const cSALARY = 79000 // From Wikipedia, late 2018
	const cOVERHEAD = 2.40
	personMonths := curve(loc)
	fmt.Printf("Development Effort Estimate, Person-Years (Person-Months) = %2.2f (%2.2f)\n", personMonths/12, personMonths)
	schedMonths := cSCHEDMULT * math.Pow(personMonths, cSCHEDEXP)
	fmt.Printf("Schedule Estimate, Years (Months)                         = %2.2f (%2.2f)\n", schedMonths/12, schedMonths)
	fmt.Printf(" (COCOMO model, Months = %2.2f * (person-months**%2.2f))\n", cSCHEDMULT, cSCHEDEXP)
	fmt.Printf("Estimated Average Number of Developers (Effort/Schedule)  = %2.2f\n", personMonths/schedMonths)
	fmt.Printf("Total Estimated Cost to Develop                           = $%d\n", int(cSALARY*(personMonths/12)*cOVERHEAD))
	fmt.Printf(" (average salary = $%d/year, overhead = %2.2f).\n", cSALARY, cOVERHEAD)
}

// listLanguages lists all languages for which we can extract line counts.
// It also performs a sanity check on identifying file extensions and
// interpreter names.
func listLanguages(lloc bool) ([]string, bool) {
	names := []string{"Perl", "Perl6"}
	suffixCounts := make(map[string]int)
	nameCounts := make(map[string]int)
	duplicates := false
	for i := range genericLanguages {
		lang := genericLanguages[i]
		if lang.property(ign) {
			continue
		}
		nameCounts[lang.Name]++
		if lang.verifier == nil {
			for _, s := range lang.Ext {
				suffixCounts[s]++
			}
		}
		for _, s := range lang.Ext {
			if suffixCounts[s] > 1 {
				fmt.Fprintf(os.Stderr, "loccount: extension %s duplicated\n", s)
				duplicates = true
			}
		}
		if nameCounts[lang.Name] == 1 {
			if !lloc || len(genericLanguages[i].St) > 0 {
				names = append(names, lang.Name)
			}
		}
	}

	sort.Strings(names)
	return names, duplicates
}

func listExtensions() {
	extensions := map[string][]string{
		"python": {".py"},
		"waf":    {"waf"},
		"perl":   {"pl", "pm"},
	}
	for i := range genericLanguages {
		lang := genericLanguages[i]
		for _, s := range lang.Ext {
			extensions[lang.Name] = append(extensions[lang.Name], s)
		}
	}

	names, duplicates := listLanguages(false)
	for i := range names {
		fmt.Printf("%s: %v\n", names[i], extensions[names[i]])
	}
	if duplicates {
		os.Exit(1)
	}
}

func dumpJSON() {
	type marshalLanguage struct {
		Name     string      // Language name
		Ext      []string    // Extensions, with leading dot
		Bcl      string      // Leader string for block comments
		Bct      string      // Trailer string for block comments
		Wcl      string      // Leader string for winged comments
		Ml       [][2]string // delimiters for multiline literals
		St       string      // Statement-terminating punctuation
		Quirks   []string
		Verifier string
	}

	for _, entry := range genericLanguages {
		var m marshalLanguage
		m.Name = entry.Name
		m.Ext = entry.Ext
		m.Bcl = entry.Bcl
		m.Bct = entry.Bct
		m.Wcl = entry.Wcl
		m.Ml = entry.Ml
		m.St = entry.St
		m.Quirks = make([]string, 0)
		for _, pair := range flagLabels {
			if entry.property(pair.flag) {
				m.Quirks = append(m.Quirks, pair.label)
			}
		}
		if entry.verifier != nil {
			m.Verifier = strings.Title(m.Verifier)
			m.Verifier = "really" + entry.Name
			m.Verifier = strings.Replace(m.Verifier, "#", "sharp", 1)
			m.Verifier = strings.Replace(m.Verifier, "-", "", 1)
		}
		s, _ := json.Marshal(m)
		r := string(s)
		r = strings.Replace(r, `"Bcl":"",`, ``, 1)
		r = strings.Replace(r, `"Bct":"",`, ``, 1)
		r = strings.Replace(r, `"Wcl":"",`, ``, 1)
		r = strings.Replace(r, `"St":"",`, ``, 1)
		r = strings.Replace(r, `"Verifier":""`, ``, 1)
		r = strings.Replace(r, `"Quirks":[],`, ``, 1)
		r = strings.Replace(r, `"Ml":[],`, ``, 1)
		r = strings.Replace(r, `,}`, `}`, 1)
		fmt.Printf("%s\n", r)
	}
}

type sortable []countRecord

func (a sortable) Len() int           { return len(a) }
func (a sortable) Swap(i int, j int)  { a[i], a[j] = a[j], a[i] }
func (a sortable) Less(i, j int) bool { return -a[i].slinecount < -a[j].slinecount }

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	var individual bool
	var unclassified bool
	var llist bool
	var slist bool
	var extensions bool
	var cocomo bool
	var json bool
	var showversion bool
	var nodocs bool
	var jsondump bool
	excludePtr := flag.String("x", "",
		"paths and directories to exclude (regexp)")
	flag.BoolVar(&individual, "i", false,
		"list counts and types for individual files")
	flag.BoolVar(&unclassified, "u", false,
		"list unclassified files")
	flag.BoolVar(&cocomo, "c", false,
		"report Cocomo-model estimation")
	flag.BoolVar(&llist, "l", false,
		"list languages that yield LLOC and exit")
	flag.BoolVar(&slist, "s", false,
		"list languages that yield SLOC and exit")
	flag.BoolVar(&extensions, "e", false,
		"list extensions associated with each language and exit")
	flag.IntVar(&debug, "d", 0,
		"set debug level")
	flag.BoolVar(&json, "j", false,
		"dump statistics in JSON format")
	flag.BoolVar(&nodocs, "n", false,
		"do not tally documentation")
	flag.BoolVar(&listignores, "g", false,
		"list generated files")
	flag.BoolVar(&transdump, "t", false,
		"Dump transitions in output.")
	flag.BoolVar(&jsondump, "J", false,
		"Dump trait table as JSON.")
	flag.BoolVar(&showversion, "V", false,
		"report version and exit")
	flag.Parse()

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	if showversion {
		fmt.Printf("loccount %s\n", version)
		return
	} else if jsondump {
		// Think twice before using this; it's undocumented for a reason.
		// While it was originally used to generate the traits.json file,
		// that file now contains information (De attributes and comments)
		// that a dump generayed this way doesn't.
		dumpJSON()
		return
	} else if slist {
		ll, duplicates := listLanguages(false)
		if !individual {
			fmt.Printf("%d: %s\n", len(ll), ll)
		} else {
			for _, lang := range ll {
				fmt.Printf("%s\n", lang)
			}
		}
		if duplicates {
			os.Exit(1)
		}
		return
	} else if llist {
		ll, _ := listLanguages(true)
		if !individual {
			fmt.Printf("%d: %s\n", len(ll), ll)
		} else {
			for _, lang := range ll {
				fmt.Printf("%s\n", lang)
			}
		}
		return
	} else if extensions {
		listExtensions()
		return
	}

	individual = individual || unclassified

	// For maximum performance, make the pipeline be as deep as the
	// number of processor we have available, that way the machine will
	// be running full-out exactly when it's filled and no sooner.
	// This makes order of output nondeterministic, which is why
	// we sometimes want to disable it.
	var chandepth int
	if individual || unclassified {
		chandepth = 0
	} else {
		chandepth = runtime.NumCPU()
	}
	pipeline = make(chan SourceStat, chandepth)

	if len(*excludePtr) > 0 {
		exclusions = regexp.MustCompile(*excludePtr)
	}
	roots := flag.Args()

	go func() {
		for i := range roots {
			fi, err := os.Stat(roots[i])
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				break
			}
			if fi.Mode().IsDir() {
				// The system filepath.Walk() works here,
				// but is slower.
				walk(roots[i], filter)
			} else {
				filter(roots[i], fi, nil)
			}
		}
		close(pipeline)
	}()

	var totals countRecord
	counts := map[string]countRecord{}

	// Mainline resumes
	for {
		st, more := <-pipeline
		if !more {
			break
		}
		if debug > 0 {
			fmt.Printf("from pipeline: %s %d %s\n",
				st.Path, st.SLOC, st.Language)
		}

		if individual {
			if !unclassified && st.SLOC > 0 {
				fmt.Printf("%s %s %d %d\n",
					st.Path, st.Language, st.SLOC, st.LLOC)
			} else if unclassified && st.SLOC == 0 {
				// Not a recognized source type,
				// nor anything we know to discard
				fmt.Println(st.Path)
			}
			continue
		}

		if st.SLOC > 0 && !(nodocs && st.markup) {
			var tmp = counts[st.Language]
			tmp.language = st.Language
			tmp.slinecount += st.SLOC
			tmp.llinecount += st.LLOC
			tmp.filecount++
			counts[st.Language] = tmp
			totals.slinecount += st.SLOC
			totals.llinecount += st.LLOC
			totals.filecount++
		}
	}

	if individual {
		return
	}

	// C headers may get reassigned based on what other languages
	// are present in the tree
	if counts["C-header"].slinecount > 0 {
		for i := range cHeaderPriority {
			if counts[cHeaderPriority[i]].slinecount > 0 {
				var tmp = counts[cHeaderPriority[i]]
				tmp.slinecount += counts["C-header"].slinecount
				counts[cHeaderPriority[i]] = tmp
				delete(counts, "C-header")
				break
			}
		}
	}

	var summary sortable
	totals.language = "all"
	if totals.filecount > 1 {
		summary = append(summary, totals)
	}
	for _, v := range counts {
		summary = append(summary, v)
	}

	sort.Sort(summary)
	for i := range summary {
		r := summary[i]
		if json {
			fmt.Printf("{\"language\":%q, \"sloc\":%d, \"lloc\":%d, \"filecount\":%d}\n",
				r.language,
				r.slinecount,
				r.llinecount,
				r.filecount)
		} else {
			fmt.Printf("%-12s SLOC=%-7d (%2.2f%%)\tLLOC=%-7d in %d files\n",
				r.language,
				r.slinecount,
				float64(r.slinecount)*100.0/float64(totals.slinecount),
				r.llinecount,
				r.filecount)
		}
	}

	if cocomo {
		reportCocomo(totals.slinecount, cocomo81)
		reportCocomo(totals.llinecount, cocomo2000)
	}
}

// end
