//////////////////////////////////////////////////////////
// BCPL: SLOC=40 LLOC=0
// Name: Peter M. Maurer
// Program: Sieve of Eratosthenes
// Due: Never
// Language: BCPL
// This program has not been tested and may be incorrect.
//////////////////////////////////////////////////////////

GET "libhdr"



LET start() = VALOF
{
	// define the sieve data structure
	LET candidates = VEC 1000
	LET i,j,k = 0,0,0
	

	FOR z = 0 TO 999 DO
	{
		// everything is potentially prime until proven otherwise
		candidates!z := 1
	}
	// Neither 1 nor 0 is prime, so flag them off
	candidates!0 := 0
	candidates!1 := 0
	// start the sieve with the integer 0
	i := 0
	WHILE i<1000 DO
	{
		// advance to the next un-crossed out number.
		// this number must be a prime
		WHILE i < 1000 & candidates!i = 0 DO
		{
			i := i + 1
		}
		// insure against running off the end of the data structure
		IF i<1000 DO
		{
			// cross out all multiples of the prime, starting with 2*p.
			j := 2
			k := i * j
			WHILE k < 1000 DO
			{
				candidates!k := 0
				j := j + 1
				k := i * j
			}
			// advance to the next candidate
			i := i + 1
		}
	}
	// all uncrossed-out numbers are prime (and only those numbers)
	// print all primes
	FOR z = 0 TO 999 DO
	{
		UNLESS candidates!z = 0 DO
		{
			writef("%n is prime*n", z)
		}
	}
	RESULTIS 0
}
